//
//  Color.swift
//  NYCSchools
//
//  Created by Naveen Swarna on 9/9/23.
//

import Foundation
import UIKit

enum Color {
    case primaryBackground
    case primaryForeground

    var color: UIColor? {
        switch self {
        case .primaryBackground:
            return UIColor(named: "primaryBackground")
        case .primaryForeground:
            return UIColor(named: "primaryForeground")
        }
    }
}
