//
//  String+Ext.swift
//  NYCSchools
//
//  Created by Naveen Swarna on 9/9/23.
//

import Foundation

extension String {
    func trim() -> String {
        trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
