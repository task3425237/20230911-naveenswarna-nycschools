//
//  BackendError.swift
//  NYCSchools
//
//  Created by Naveen Swarna on 9/9/23.
//

import Foundation

/// BackendError enum to handle errors
enum BackendError: Error {
    case invalidUrl
    case invalidJson
}
