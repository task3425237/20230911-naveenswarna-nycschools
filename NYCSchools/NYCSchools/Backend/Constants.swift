//
//  Constants.swift
//  NYCSchools
//
//  Created by Naveen Swarna on 9/9/23.
//

import Foundation

struct Constants {
    static let baseURL = "https://data.cityofnewyork.us/resource"
}
