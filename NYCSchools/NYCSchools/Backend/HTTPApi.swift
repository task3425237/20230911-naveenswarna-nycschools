//
//  HTTPApi.swift
//  NYCSchools
//
//  Created by Naveen Swarna on 9/9/23.
//

import Foundation

/// HTTPApi to create all api and required parameters
enum HTTPApi {
    case schoolList
    case schoolSATScore

    var endpoint: String {
        switch self {
        case .schoolList:
            return "/s3k6-pzi2.json"
        case .schoolSATScore:
            return "/f9bf-2cp4.json"
        }
    }

    var httpMethod: String {
        "GET"
    }

    var requestHeaders: [String: String]? {
        nil
    }

    var requestBody: Data? {
        nil
    }
}


//53k6-pzi2
